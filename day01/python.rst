=======
Python
=======

講義ではPython/Rを使う(予定)

Pythonのインストール
====================

- Ubuntu/Debian/Mac

デフォルトで入ってる2.7.3を使用

- Gentoo

( ◠‿◠ )☛ Gentoo使ってるなら自前でビルドできるだろ甘えんな

- Windows

公式で配布してるインストーラが楽

周辺の開発環境
--------------

後述するライブラリをインストールするために行う。

まずいろいろなライブラリをインストールするために
pipをインストールしよう。

- Ubuntu/Debian

apt-get install python-pip

- Windows

TODO: だれかかいちくりー

ライブラリ
==========

パターン認識を行うのに便利なライブラリ

ipython
--------

直接パターン認識とは関係ないが、とても便利なツール。

高機能な対話型インターフェース、ちょっとした挙動を調べたりするのに便利。

pip install ipython

scikit-learn
-------------

http://scikit-learn.org/stable/

主要な機械学習アルゴリズムがまとまっているライブラリ。

インストール手順が多少面倒(依存ライブラリが多い)

- Ubuntu/Debian

apt-get install build-essential python-dev python-numpy python-setuptools python-scipy libatlas-dev python-matplotlib

てはじめにこれらをインストールしておこう

pip install scikit-learn

